﻿namespace ServerTimeChecker
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    ///   The server.
    /// </summary>
    [Serializable]
    public class Server : ISerializable
    {
        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="Server" /> class.
        /// </summary>
        public Server()
        {
        }

        /// <summary>Initializes a new instance of the <see cref="Server"/> class.</summary>
        /// <param name="info">The info. </param>
        /// <param name="ctxt">The ctxt. </param>
        public Server(SerializationInfo info, StreamingContext ctxt)
        {
            // Get the values from info and assign them to the appropriate properties
            this.Name = (string)info.GetValue("Name", typeof(string));
            this.UrlToCheck = (string)info.GetValue("ServerUrl", typeof(string));
            this.Tolerance = (TimeSpan)info.GetValue("Tolerance", typeof(TimeSpan));
            this.Difference = (TimeSpan)info.GetValue("Difference", typeof(TimeSpan));
            this.LastCheckedTime = (DateTime)info.GetValue("LastCheckedTime", typeof(DateTime));
        }

        #endregion

        #region Public Properties

        /// <summary>Gets or sets the difference.</summary>
        public TimeSpan Difference { get; set; }

        /// <summary>Gets or sets the last checked time.</summary>
        public DateTime LastCheckedTime { get; set; }

        /// <summary>
        ///   Gets or sets the server name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///   Gets or sets the server time difference tolerance.
        /// </summary>
        public TimeSpan Tolerance { get; set; }

        /// <summary>
        ///   Gets or sets the server url to check.
        /// </summary>
        public string UrlToCheck { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>The get object data.</summary>
        /// <param name="info">The info. </param>
        /// <param name="context">The context. </param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", this.Name);
            info.AddValue("ServerUrl", this.UrlToCheck);
            info.AddValue("Tolerance", this.Tolerance);
            info.AddValue("Difference", this.Difference);
            info.AddValue("LastCheckedTime", this.LastCheckedTime);
        }

        /// <summary>
        ///   The to string.
        /// </summary>
        /// <returns> The <see cref="string" /> . </returns>
        public override string ToString()
        {
            return string.Concat(
                "Name: ", 
                this.Name, 
                " URL: ", 
                this.UrlToCheck, 
                " Tolerance: ", 
                this.Tolerance, 
                " Difference: ", 
                this.Difference, 
                " LastCheckedTime: ", 
                this.LastCheckedTime);
        }

        #endregion
    }
}