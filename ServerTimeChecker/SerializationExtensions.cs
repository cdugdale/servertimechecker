﻿namespace ServerTimeChecker
{
    using System.IO;
    using System.Runtime.Serialization;
    using System.Xml;

    /// <summary>The serialization extensions.</summary>
    public static class SerializationExtensions
    {
        #region Public Methods and Operators

        /// <summary>The deserialize.</summary>
        /// <param name="serialized">The serialized.</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>The <see cref="T"/>.</returns>
        public static T Deserialize<T>(this string serialized)
        {
            var serializer = new DataContractSerializer(typeof(T));
            using (var reader = new StringReader(serialized))
            using (var stm = new XmlTextReader(reader))
            {
                return (T)serializer.ReadObject(stm);
            }
        }

        /// <summary>The serialize.</summary>
        /// <param name="obj">The obj.</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>The <see cref="string"/>.</returns>
        public static string Serialize<T>(this T obj)
        {
            var serializer = new DataContractSerializer(obj.GetType());
            using (var writer = new StringWriter())
            using (var stm = new XmlTextWriter(writer))
            {
                serializer.WriteObject(stm, obj);
                return writer.ToString();
            }
        }

        #endregion
    }
}