﻿namespace ServerTimeChecker
{
    partial class ServerTimeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServerName = new System.Windows.Forms.Label();
            this.btnCheckNow = new System.Windows.Forms.Button();
            this.lblTol = new System.Windows.Forms.Label();
            this.lblDiff = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUrlToCheck = new System.Windows.Forms.Label();
            this.lblLastChecked = new System.Windows.Forms.Label();
            this.lblDifference = new System.Windows.Forms.Label();
            this.lblTolerance = new System.Windows.Forms.Label();
            this.lblChecked = new System.Windows.Forms.Label();
            this.lblUrl = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ServerName
            // 
            this.ServerName.AutoSize = true;
            this.ServerName.BackColor = System.Drawing.SystemColors.Control;
            this.ServerName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ServerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ServerName.Location = new System.Drawing.Point(56, 10);
            this.ServerName.Name = "ServerName";
            this.ServerName.Size = new System.Drawing.Size(79, 15);
            this.ServerName.TabIndex = 0;
            this.ServerName.Text = "Server Name";
            // 
            // btnCheckNow
            // 
            this.btnCheckNow.Location = new System.Drawing.Point(324, 7);
            this.btnCheckNow.Name = "btnCheckNow";
            this.btnCheckNow.Size = new System.Drawing.Size(75, 23);
            this.btnCheckNow.TabIndex = 1;
            this.btnCheckNow.Text = "Check now";
            this.btnCheckNow.UseVisualStyleBackColor = true;
            this.btnCheckNow.Click += new System.EventHandler(this.BtnCheckNowClick);
            // 
            // lblTol
            // 
            this.lblTol.AutoSize = true;
            this.lblTol.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTol.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblTol.Location = new System.Drawing.Point(603, 10);
            this.lblTol.Name = "lblTol";
            this.lblTol.Size = new System.Drawing.Size(27, 15);
            this.lblTol.TabIndex = 2;
            this.lblTol.Text = "Tol:";
            // 
            // lblDiff
            // 
            this.lblDiff.AutoSize = true;
            this.lblDiff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiff.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblDiff.Location = new System.Drawing.Point(215, 10);
            this.lblDiff.Name = "lblDiff";
            this.lblDiff.Size = new System.Drawing.Size(28, 15);
            this.lblDiff.TabIndex = 3;
            this.lblDiff.Text = "Diff:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblUrlToCheck);
            this.panel1.Controls.Add(this.lblLastChecked);
            this.panel1.Controls.Add(this.lblDifference);
            this.panel1.Controls.Add(this.lblTolerance);
            this.panel1.Controls.Add(this.lblChecked);
            this.panel1.Controls.Add(this.lblUrl);
            this.panel1.Controls.Add(this.lblDiff);
            this.panel1.Controls.Add(this.lblTol);
            this.panel1.Controls.Add(this.btnCheckNow);
            this.panel1.Controls.Add(this.ServerName);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(953, 36);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "Server:";
            // 
            // lblUrlToCheck
            // 
            this.lblUrlToCheck.AutoSize = true;
            this.lblUrlToCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUrlToCheck.Location = new System.Drawing.Point(699, 10);
            this.lblUrlToCheck.Name = "lblUrlToCheck";
            this.lblUrlToCheck.Size = new System.Drawing.Size(171, 15);
            this.lblUrlToCheck.TabIndex = 9;
            this.lblUrlToCheck.Text = "There should be a URL here...";
            // 
            // lblLastChecked
            // 
            this.lblLastChecked.AutoSize = true;
            this.lblLastChecked.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastChecked.Location = new System.Drawing.Point(475, 10);
            this.lblLastChecked.Name = "lblLastChecked";
            this.lblLastChecked.Size = new System.Drawing.Size(37, 15);
            this.lblLastChecked.TabIndex = 8;
            this.lblLastChecked.Text = "--:--.--";
            // 
            // lblDifference
            // 
            this.lblDifference.AutoSize = true;
            this.lblDifference.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDifference.Location = new System.Drawing.Point(249, 10);
            this.lblDifference.Name = "lblDifference";
            this.lblDifference.Size = new System.Drawing.Size(15, 15);
            this.lblDifference.TabIndex = 7;
            this.lblDifference.Text = "--";
            // 
            // lblTolerance
            // 
            this.lblTolerance.AutoSize = true;
            this.lblTolerance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTolerance.Location = new System.Drawing.Point(628, 10);
            this.lblTolerance.Name = "lblTolerance";
            this.lblTolerance.Size = new System.Drawing.Size(24, 15);
            this.lblTolerance.TabIndex = 6;
            this.lblTolerance.Text = "Tol";
            // 
            // lblChecked
            // 
            this.lblChecked.AutoSize = true;
            this.lblChecked.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChecked.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblChecked.Location = new System.Drawing.Point(405, 10);
            this.lblChecked.Name = "lblChecked";
            this.lblChecked.Size = new System.Drawing.Size(70, 15);
            this.lblChecked.TabIndex = 5;
            this.lblChecked.Text = "Last Check:";
            // 
            // lblUrl
            // 
            this.lblUrl.AutoSize = true;
            this.lblUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUrl.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblUrl.Location = new System.Drawing.Point(658, 10);
            this.lblUrl.Name = "lblUrl";
            this.lblUrl.Size = new System.Drawing.Size(35, 15);
            this.lblUrl.TabIndex = 4;
            this.lblUrl.Text = "URL:";
            // 
            // ServerTimeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.panel1);
            this.Name = "ServerTimeControl";
            this.Size = new System.Drawing.Size(959, 42);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ServerName;
        private System.Windows.Forms.Button btnCheckNow;
        private System.Windows.Forms.Label lblTol;
        private System.Windows.Forms.Label lblDiff;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUrl;
        private System.Windows.Forms.Label lblTolerance;
        private System.Windows.Forms.Label lblChecked;
        private System.Windows.Forms.Label lblDifference;
        private System.Windows.Forms.Label lblLastChecked;
        private System.Windows.Forms.Label lblUrlToCheck;
        private System.Windows.Forms.Label label1;

    }
}
