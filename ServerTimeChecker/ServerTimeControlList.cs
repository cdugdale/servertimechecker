﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ServerTimeChecker
{
    public partial class ServerTimeControlList : UserControl
    {
        public ServerTimeControlList()
        {
            InitializeComponent();
        }
        
        public void PopulateServerList(List<ServerTimeControlBase> serverTimeControls)
        {
            this.panel1.Controls.Clear();
            this.panel1.Height = 6;

            try
            {




                if (serverTimeControls.Count > 0)
                {
                    Point loc = new Point(3, 4);


                    this.panel1.Width = serverTimeControls[0] == null ? 100 : serverTimeControls[0].Width + 23;

                    // Add eaech of the controls to the panel to display them
                    //for (int i = 0; i < serverTimeControls.Count; i++)
                    //{

                    //    serverTimeControls[i].IndexOnForm = i; //serverTimeControls.IndexOf(serverTimeControls[i]);

                    //    this.panel1.Controls.Add(serverTimeControls[i]);

                    //    serverTimeControls[i].Location = loc;
                    //    loc.Y = loc.Y + 44;

                    //    this.panel1.Height += 44;
                    //}
                    foreach (ServerTimeControlBase serverTimeControl in serverTimeControls)
                    {
                       // serverTimeControl.indexOnForm = serverTimeControls.IndexOf(serverTimeControl);

                        this.panel1.Controls.Add(serverTimeControl);

                        serverTimeControl.Location = loc;
                        loc.Y = loc.Y + 44;

                        this.panel1.Height += 44;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
