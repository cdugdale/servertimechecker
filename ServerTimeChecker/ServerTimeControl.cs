﻿using System;


namespace ServerTimeChecker
{
    public partial class ServerTimeControl : ServerTimeControlBase
    {
        public int? indexOnForm = null;

        public int? IndexOnForm
        {
            get
            {
                return this.indexOnForm;
            }
            set
            {
                this.indexOnForm = value;
            }
        }

        public ServerTimeControl()
        {
            InitializeComponent();
        }

        internal ServerTimeControl(Server server)
        {
            InitializeComponent();
            Populate(server);
        }


        public event EventHandler CheckNowButtonClicked;


        internal void Populate(Server server)
        {
            this.ServerName.Text = server.Name;
            this.lblUrlToCheck.Text = server.UrlToCheck;
            this.lblTolerance.Text = string.Concat(server.Tolerance.TotalSeconds.ToString(), "s");
            this.lblDifference.Text = string.Concat(Convert.ToInt64(server.Difference.TotalSeconds).ToString(), "s");
            this.lblLastChecked.Text = server.LastCheckedTime.ToString();
        }

        private void BtnCheckNowClick(object sender, EventArgs e)
        {
            if (this.CheckNowButtonClicked != null)
            {
                /* todo try this with sending server name and have a method to 
                 * find the control by server name to refresh just that one. 
                 */
                //this.CheckNowButtonClicked(this.indexOnForm, e);


                this.CheckNowButtonClicked(this.ServerName.Text, e);
            }//read from the server name label and use that ot do the check and also the tolerance
        }

    }
}
