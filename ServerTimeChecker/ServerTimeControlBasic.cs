﻿namespace ServerTimeChecker
{
    using System;

    /// <summary>The server time control basic.</summary>
    public partial class ServerTimeControlBasic : ServerTimeControlBase
    {
        #region Constructors and Destructors

        /// <summary>Initializes a new instance of the <see cref="ServerTimeControlBasic"/> class.</summary>
        public ServerTimeControlBasic()
        {
            this.InitializeComponent();
        }

        /// <summary>Initializes a new instance of the <see cref="ServerTimeControlBasic"/> class.</summary>
        /// <param name="server">The server.</param>
        internal ServerTimeControlBasic(Server server)
        {
            this.InitializeComponent();
            this.Populate(server);
        }

        #endregion

        #region Public Events

        /// <summary>The delete button clicked.</summary>
        public event EventHandler DeleteButtonClicked;

        /// <summary>The edit button clicked.</summary>
        public event EventHandler EditButtonClicked;

        #endregion

        #region Public Methods and Operators

        /// <summary>The btn delete click.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        public void BtnDeleteClick(object sender, EventArgs e)
        {
            if (this.DeleteButtonClicked != null)
            {
                this.DeleteButtonClicked(this.ServerName.Text, e);
            }
        }

        /// <summary>The btn edit click.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        public void BtnEditClick(object sender, EventArgs e)
        {
            if (this.EditButtonClicked != null)
            {
                this.EditButtonClicked(this.ServerName.Text, e);
            }
        }

        #endregion

        #region Methods

        /// <summary>The populate.</summary>
        /// <param name="server">The server.</param>
        internal void Populate(Server server)
        {
            this.ServerName.Text = server.Name;
        }

        #endregion

        // CustomControl.cs
        //// Assumes a Button 'myButton' has been added through the designer

        //// we need a delegate definition to type our event
        // public delegate void ButtonClickHandler(object sender, EventArgs e);

        //// declare the public event that other classes can subscribe to
        // public event ButtonClickHandler ButtonClickEvent;

        //// wire up the internal button click event to trigger our custom event
        // public void BtnEditClick(object sender, EventArgs e)
        // {
        // if (ButtonClickEvent != null)
        // {
        // ButtonClickEvent(sender, e);
        // }
        // }
    }
}