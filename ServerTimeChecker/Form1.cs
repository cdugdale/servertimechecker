﻿namespace ServerTimeChecker
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Mail;
    using System.Timers;
    using System.Windows.Forms;
    using NLog;
    using ServerTimeChecker.Logger;

    /// <summary>
    ///   The form 1.
    /// </summary>
    public partial class Form1 : Form
    {
        // private Dictionary<string, bool> fileExtensions = new Dictionary<string, bool>();

        // private Configuration config = null;
        #region Fields


        private System.Timers.Timer timeCount = new System.Timers.Timer();

        //private static Log logger = LogManager.GetCurrentClassLogger();

        /// <summary>The config.</summary>
        private Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

        /// <summary>
        ///   The serialized.
        /// </summary>
        //private string serialized;

        BackgroundWorker m_oWorker;



        public string baseDirectory = AppDomain.CurrentDomain.BaseDirectory ;
        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="Form1" /> class.
        /// </summary>
        public Form1()
        {
            try
            {
                Log.Instance.Trace("Entering form1");

                this.InitializeComponent();
                UI.Initialize(this);
                this.LoadServers();
                
                int minutes = 1;
                timeCount.Interval = minutes * 6000; // to convert this to milliseconds which is exepcted for this method * 60000
                timeCount.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                timeCount.Enabled = true;
                timeCount.Start();

                this.m_oWorker = new BackgroundWorker();
                //
                // Create a background worker thread that ReportsProgress &
                // SupportsCancellation
                // Hook up the appropriate events.
                this.m_oWorker.DoWork += new DoWorkEventHandler(this.m_oWorker_DoWork);
                //m_oWorker.ProgressChanged += new ProgressChangedEventHandler (m_oWorker_ProgressChanged);
                this.m_oWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.m_oWorker_RunWorkerCompleted);
                //m_oWorker.WorkerReportsProgress = true;
                //m_oWorker.WorkerSupportsCancellation = true;

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }

        

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            //this.toolStripStatusLabel1.Text = "Time check automatically initiated at: " + DateTime.Now.ToShortTimeString();
            if (!m_oWorker.IsBusy)
                m_oWorker.RunWorkerAsync();
            else
                MessageBox.Show("Can't run the worker twice!");
            //this.m_oWorker.RunWorkerAsync();
        }  

        #endregion

        // private DialogResult PreClosingConfirmation()
        // {
        // DialogResult res = MessageBox.Show(
        // "Are you sure you want to exit?", "Confirm exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        // return res;
        // }
        #region Methods
        void m_oWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // The background process is complete. We need to inspect
            // our response to see if an error occurred, a cancel was
            // requested or if we completed successfully.  
            //if (e.Cancelled)
            //{
            //    lblStatus.Text = "Task Cancelled.";
            //}

            // Check to see if an error occurred in the background process.

            if (e.Error != null)
            {
                Log.Instance.Error("Thread error while performing background operation");
                MessageBox.Show("Error while performing background operation."); 
            }
            else
            {
                // Everything completed normally.

                //MessageBox.Show("Task Completed...");

                //toolStripStatusLabel1.Text = "Last full check initiated at: " + DateTime.Now.ToShortTimeString();

                UI.Invoke(() => { this.toolStripStatusLabel1.Text = "Last full check initiated at: " + DateTime.Now.ToShortTimeString(); });


                //if (toolStripStatusLabel1.InvokeRequired)
                //    serverTimeControlList1.Invoke((Action)(() => toolStripStatusLabel1.Text = "Last full check initiated at: " + DateTime.Now.ToShortTimeString()));
                //else
                //{
                //    toolStripStatusLabel1.Text = "Last full check initiated at: " + DateTime.Now.ToShortTimeString();
                //}


                if (serverTimeControlList1.InvokeRequired)
                    serverTimeControlList1.Invoke((Action)(() => LoadServers()));
                else
                {
                    LoadServers();
                }

            }


            //Change the status of the buttons on the UI accordingly
            //btnStartAsyncOperation.Enabled = true;
            //btnCancel.Enabled = false;
        }

        
        /// <summary>
        /// Time consuming operations go here
        /// i.e. Database operations,Reporting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void m_oWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            IEnumerable<Server> servers = ServerHelpers.GetOrderedServerList(this.config);

            foreach (Server server in servers)
            {
                this.CheckServerTime(server);
            }

            // The sender is the BackgroundWorker object we need it to
            // report progress and check for cancellation.
            //NOTE : Never play with the UI thread here...
            //Object sender1 = e.Argument as Object;   
            
            
            //CheckServerTime(sender1);
            

            //Report 100% completion on operation completed
           // m_oWorker.ReportProgress(100);
        }

        private void btnStartAsyncOperation_Click(object sender, EventArgs e)
        {
           
            // Kickoff the worker thread to begin it's DoWork function.

            this.toolStripStatusLabel1.Text = "Time check manually initiated at: " + DateTime.Now.ToShortTimeString();
            this.m_oWorker.RunWorkerAsync();

        }

        
        /// <summary>The on form closed.</summary>
        /// <param name="e">The e.</param>
        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            this.config.Save(ConfigurationSaveMode.Modified);
            this.config.Save();
            base.OnFormClosed(e);
        }

        /// <summary>The check server time.</summary>
        /// <param name="sender">The sender.</param>
        private void CheckServerTime(Server serverToCheck)
        {
            try
            {
                string timeFromServer = ServerHelpers.getRemoteServerTime(serverToCheck);

                if (timeFromServer.Length > 0)
                {
                    DateTime serverTime = DateTime.Parse(timeFromServer);

                    // this.GetTimeDifference(DateTime.Now, serverTime);

                    serverToCheck.Difference = ServerHelpers.GetTimeDifference(DateTime.Now, serverTime);
                    serverToCheck.LastCheckedTime = DateTime.Now;

                    if (ServerHelpers.DifferenceWithinTolerance(serverToCheck.Difference, serverToCheck.Tolerance)
                        == false)
                    {

                        /*Method One and two both work for sending from outlook, need to test which is the most reliable and which works from Gandalf. */
                        /*Method One */
                        SmtpClient smtpCLient = new SmtpClient("smtp.live.com");
                        MailMessage message = new MailMessage();
                        message.From = new MailAddress("cdugdale@outlook.com");
                        message.To.Add("christopher.dugdale@gmail.com");
                        message.Subject = "Server Time Discrepency: " + serverToCheck.Name;
                        message.IsBodyHtml = true;
                        //string htmlBody;
                        //htmlBody =
                          message.Body = string.Format(
                                "The server time on {0} is outside the allowed tolerance of {1}s. \n\nThe server time was \n{2} when the comparison time was \n{3}.",
                                serverToCheck.Name,
                                serverToCheck.Tolerance.Seconds,
                                serverTime,
                                serverToCheck.LastCheckedTime);
                        //message.Body = htmlBody;
                        smtpCLient.Port = 587;
                        smtpCLient.UseDefaultCredentials = false;
                        smtpCLient.Credentials = new NetworkCredential("cdugdale@outlook.com", "JKH1a2aw21r23QdQ");
                        smtpCLient.EnableSsl = true;


                        /*Method Two */

                        //var fromAddress = new MailAddress("cdugdale@outlook.com", "From Name");
                        //var toAddress = new MailAddress("christopher.dugdale@gmail.com", "To Name");
                        //const string fromPassword = "JKH1a2aw21r23QdQ";
                        //const string subject = "test";
                        //const string body = "Hey now!!";
                        //var smtp = new SmtpClient
                        //    {
                        //        Host = "smtp.live.com",
                        //        Port = 587,
                        //        EnableSsl = true,
                        //        DeliveryMethod = SmtpDeliveryMethod.Network,
                        //        UseDefaultCredentials = false,
                        //        Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                        //        Timeout = 20000
                        //    };
                        //using (
                        //    var message = new MailMessage(fromAddress, toAddress) { Subject = subject, Body = body }
                        //    )
                        //{
                        //    smtp.Send(message);
                        //}
                            
                        //client.Send(message);
                            
                        try
                        {
                            smtpCLient.Send(message);
                            System.Threading.Thread.Sleep(2000); //just to make sure that the emails have a chance to send without screwing thread, might be totally unecessary. 
                            Log.Instance.Debug("Email sent to " + message.To + "");
                        } 
                        catch (SmtpFailedRecipientsException ex)
                        {
                            for (int i = 0; i < ex.InnerExceptions.Length; i++)
                            {
                                SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                                if (status == SmtpStatusCode.MailboxBusy ||
                                    status == SmtpStatusCode.MailboxUnavailable)
                                {
                                    Log.Instance.Warn("Delivery failed - retrying in 5 seconds.");
                                    System.Threading.Thread.Sleep(5000);
                                    smtpCLient.Send(message);
                                }
                                else
                                {
                                    Log.Instance.Warn("Failed to deliver message to {0}",
                                        ex.InnerExceptions[i].FailedRecipient);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Instance.Error("Exception caught in RetryIfBusy(): {0}",
                                    ex.ToString());
                        }
                    }

                    ServerHelpers.SaveServer(this.config, serverToCheck);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

            }
        }

        //}

        /// <summary>The control check now button clicked.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void ControlCheckNowButtonClicked(object sender, EventArgs e)
        {
            //todo: sender isn't the server at the moment. so make it so!


            // config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            Server server = ServerHelpers.GetServerByName(this.config, sender.ToString());

           // this.m_oWorker.RunWorkerAsync(server);

            CheckServerTime(server);
            this.LoadServers();
        }

        /// <summary>The load servers.</summary>
        private void LoadServers()
        {
            // config = ConfigurationManager.AppSettings.
            this.config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

            //List<Server> serverList = new List<Server>();
            List<ServerTimeControlBase> serverTimeControls = new List<ServerTimeControlBase>();

            if (this.config.AppSettings.Settings.AllKeys.Length > 0)
            {
                IEnumerable<Server> orderedServerList = ServerHelpers.GetOrderedServerList(this.config);

                // Add each server to a server time control and add those to a list of servertimcontrols
                foreach (Server server in orderedServerList)
                {
                    ServerTimeControl stc = new ServerTimeControl(server);
                    serverTimeControls.Add(stc);
                    stc.CheckNowButtonClicked += this.ControlCheckNowButtonClicked;
                }
            }

            this.serverTimeControlList1.PopulateServerList(serverTimeControls);
        }

        /// <summary>The edit tool strip menu item_ click.</summary> 
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (new CenterWinDialog(this))
            {
                var dialog = new EditServers();
                dialog.ShowDialog(this);
            }

            this.LoadServers();
        }

        /// <summary>The new server tool strip menu item_ click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void newServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.AddNewServer();
            this.LoadServers();
        }

        private void AddNewServer()
        {
            Server newServer = new Server();

            using (new CenterWinDialog(this))
            {
                var dialog = new AddEditServer(Enums.AddEditMode.Add);
                dialog.ShowDialog(this);
                newServer = dialog.GetServerDetails();
            }

            if (newServer.Name.Length > 0)
            {

                Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
                config.AppSettings.Settings.Add(newServer.Name, newServer.Serialize());
                config.Save(ConfigurationSaveMode.Full);
                config.Save();

                
            }
        }

        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void viewLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(baseDirectory + "logs\\" + "applog" + DateTime.Today.ToString("yyyy-MM-dd") + ".txt");
        }

        private void viewOtherLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(baseDirectory + "logs\\");
        }

    }
}