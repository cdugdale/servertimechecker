﻿namespace ServerTimeChecker
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    /// <summary>The server helpers.</summary>
    public class ServerHelpers
    {
        #region Methods

        /// <summary>The add new server.</summary>
        /// <param name="dialog">The dialog.</param>
        /// <param name="config">The config.</param>
        internal static void AddNewServer(AddEditServer dialog, Configuration config)
        {
            Server newServer = new Server();

            config.AppSettings.Settings.Add(newServer.Name, newServer.Serialize());
            config.Save(ConfigurationSaveMode.Full);
            config.Save();
        }

        /// <summary>The difference within tolerance.</summary>
        /// <param name="difference">The difference.</param>
        /// <param name="tolerance">The tolerance.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        internal static bool DifferenceWithinTolerance(TimeSpan difference, TimeSpan tolerance)
        {
            if (difference > tolerance)
            {
                return false;
            }
            else if (difference <= tolerance)
            {
                return true;
            }

            // in case of error returns false for safety
            return false;
        }

        /// <summary>The get ordered server list.</summary>
        /// <param name="config">The config.</param>
        /// <param name="serverList">The server list.</param>
        /// <returns>The <see cref="IEnumerable"/>.</returns>
        internal static IEnumerable<Server> GetOrderedServerList(Configuration config)
        {
            List<Server> serverList = new List<Server>();
            foreach (string key in config.AppSettings.Settings.AllKeys)
            {
                if (key != null)
                {
                    Server server = config.AppSettings.Settings[key].Value.Deserialize<Server>();
                    serverList.Add(server);
                }
            }

            IEnumerable<Server> orderedServerList = serverList.OrderBy(p => p.Name);
            return orderedServerList;
        }

        /// <summary>The get server by name.</summary>
        /// <param name="config">The config.</param>
        /// <param name="serverName">The server name.</param>
        /// <returns>The <see cref="Server"/>.</returns>
        internal static Server GetServerByName(Configuration config, string serverName)
        {
            Server server = new Server();
            
            foreach (string key in config.AppSettings.Settings.AllKeys)
            {
                if (key == serverName)
                {
                    server = config.AppSettings.Settings[key].Value.Deserialize<Server>();
                }
            }

            return server;
        }

        /// <summary>The get time difference.</summary>
        /// <param name="localTime">The local time.</param>
        /// <param name="remoteTime">The remote time.</param>
        /// <returns>The <see cref="TimeSpan"/>.</returns>
        internal static TimeSpan GetTimeDifference(DateTime localTime, DateTime remoteTime)
        {
            TimeSpan difference = localTime - remoteTime;
            return difference.Duration();
        }

        /// <summary>The save server.</summary>
        /// <param name="config">The config.</param>
        /// <param name="server">The server.</param>
        internal static void SaveServer(Configuration config, Server server)
        {
            config.AppSettings.Settings[server.Name].Value = server.Serialize();
            config.Save(ConfigurationSaveMode.Full);
            config.Save();
        }

        /// <summary>The get remote server time.</summary>
        /// <param name="config">The config.</param>
        /// <param name="sender">The sender.</param>
        /// <returns>The <see cref="string"/>.</returns>
        internal static string getRemoteServerTime(Server server)
        {
            //Server server = GetServerByName(config, sender.ToString());
            WebPageReader pageReader = new WebPageReader();

            return pageReader.ReadPage(server.UrlToCheck);
        }

        #endregion
    }
}