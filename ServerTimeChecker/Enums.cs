﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerTimeChecker
{
    public class Enums
    {
        /// <summary>The mode.</summary>
        public enum AddEditMode
        {
            /// <summary>The add.</summary>
            Add, 

            /// <summary>The edit.</summary>
            Edit
        }
    }
}
