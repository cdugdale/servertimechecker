﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 using System.Net;
using System.Windows.Forms;

namespace ServerTimeChecker
{
    using ServerTimeChecker.Logger;

    internal class WebPageReader
    {   
        //option 1

        //private String url = "http://www.google.com";
        private String urlOfPage = string.Empty;
        private String result = string.Empty;
        private WebClient client = new WebClient();


        //public WebPageReader(string url)
        //{
        //    //this.urlOfPage = url;

        //}





        public string ReadPage(string urlOfPage)
        {
            try
            {

                if (client.DownloadString(urlOfPage).Length > 0)
                {
                    return client.DownloadString(urlOfPage);    
                }
                else
                {
                    return "";
                }
                return "test";
            }
            catch(NotSupportedException ex)
            {
                Log.Instance.Error("Format of URL to check was not in a supported format. Incorrect URL is: ", urlOfPage);
                //throw;
            }
            catch (Exception ex)
            {
                MessageBox.Show("could not find file");
                //Log.Instance.Error("Format of URL to check was not in a supported format. Incorrect URL is: ", urlOfPage);
                //throw;
            }
           
            return string.Empty;
        }


        //option 2
        //WebClient web = new WebClient();
        //System.IO.Stream stream = web.OpenRead("http://www.yoursite.com/resource.txt");
        //using (System.IO.StreamReader reader = new System.IO.StreamReader(stream))
        //{
        //    String text = reader.ReadToEnd();
        //}

    }
}
