﻿namespace ServerTimeChecker
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Windows.Forms;
    using System.Linq;

    /// <summary>
    ///   The edit servers.
    /// </summary>
    public partial class EditServers : Form
    {
        #region Constructors and Destructors

        /// <summary>Initializes a new instance of the <see cref="EditServers"/> class. 
        ///   The config.</summary>
        /// <summary>Initializes a new instance of the <see cref="EditServers"/> class.</summary>
        public EditServers()
        {
            this.InitializeComponent();
            this.LoadServers();
        }

        #endregion

        #region Methods

        /// <summary>The control delete button clicked.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void ControlDeleteButtonClicked(object sender, EventArgs e)
        {
            Server server = new Server();
            server.Name = sender.ToString();

            DialogResult dialogResult =
                MessageBox.Show(
                    "Are you sure you want to delete the server " + server.Name + "\n\nThis cannot be undone", 
                    "Confirm Delete", 
                    MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
                config.AppSettings.Settings.Remove(server.Name);
                config.Save(ConfigurationSaveMode.Full);
                config.Save();

                this.LoadServers();
            }
        }

        /// <summary>The control edit button clicked.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void ControlEditButtonClicked(object sender, EventArgs e)
        {
            using (new CenterWinDialog(this))
            {
                Server server = new Server();
                Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);


                //Server server = ConfigurationManager.AppSettings[sender.ToString()].Deserialize<Server>();

                foreach (string key in config.AppSettings.Settings.AllKeys)
                {
                    if (key == sender.ToString())
                    {
                        server = config.AppSettings.Settings[key].Value.Deserialize<Server>();
                    }
                }

                AddEditServer addEditServerDialog = new AddEditServer(Enums.AddEditMode.Edit);
                addEditServerDialog.Text = "Edit " + server.Name;

                addEditServerDialog.TxtServerName.Text = server.Name;
                addEditServerDialog.TxtServerUrl.Text = server.UrlToCheck;
                addEditServerDialog.TxtServerTolerance.Text = server.Tolerance.TotalSeconds.ToString();

                addEditServerDialog.SaveButtonClicked += this.SaveButtonClicked;
                addEditServerDialog.ShowDialog(this);
                addEditServerDialog.SaveButtonClicked -= this.SaveButtonClicked;
            }
        }

        /// <summary>
        ///   The load servers.
        /// </summary>
        private void LoadServers()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

            //List<Server> serverList = new List<Server>();
            List<ServerTimeControlBase> serverTimeControls = new List<ServerTimeControlBase>();
            //IEnumerable<Server> orderedServerList;

            try
            {

                if (config.AppSettings.Settings.AllKeys.Length > 0)
                {
                    // this can be condensed into one method
                    //foreach (string key in config.AppSettings.Settings.AllKeys)
                    //{

                    //    if (key != null)
                    //    {
                    //        Server server = config.AppSettings.Settings[key].Value.Deserialize<Server>();
                    //        serverList.Add(server);
                    //    }
                    //}

                    //IEnumerable<Server> orderedServerList = serverList.OrderBy(p => p.Name);
                    IEnumerable<Server> orderedServerList = ServerHelpers.GetOrderedServerList(config);

                    //if (serverList.Count > 1)
                    //{
                    //    orderedServerList = serverList.OrderBy(p => p.Name);
                    //}
                    //else
                    //{
                    //    orderedServerList = serverList;
                    //}

                    // Add each server to a server time control and add those to a list of servertimecontrols
                    foreach (Server server in orderedServerList)
                    {
                        ServerTimeControlBasic stc = new ServerTimeControlBasic(server);
                        serverTimeControls.Add(stc);
                        stc.EditButtonClicked += this.ControlEditButtonClicked;
                        stc.DeleteButtonClicked += this.ControlDeleteButtonClicked;
                    }


                    this.serverTimeControlList1.PopulateServerList(serverTimeControls);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        /// <summary>The save button clicked.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void SaveButtonClicked(object sender, EventArgs e)
        {
            Server server = sender as Server;

            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings[server.Name].Value = server.Serialize();
            config.Save(ConfigurationSaveMode.Full);
            config.Save();

            this.LoadServers();
        }

        #endregion

        // private void CancelButtonClicked(object sender, EventArgs e)
        // {

        // }

        // <summary>The new server tool strip menu item_ click.</summary>
        // <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        // private void ControlEditButtonClick(object sender, EventArgs e)
        // {
        // using (new CenterWinDialog(this))
        // {
        // var dialog = new AddEditServer();
        // dialog.Text = "wibble";
        // dialog.ShowDialog(this);
        // }

        // this.LoadServers();
        // }
    }
}