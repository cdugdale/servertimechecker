﻿namespace ServerTimeChecker
{
    using System;
    using System.Configuration;
    using System.Windows.Forms;

    /// <summary>
    ///   The new server.
    /// </summary>
    public partial class AddEditServer : Form
    {
        #region Fields

        /// <summary>
        ///   The AddEditMode.
        /// </summary>
        private Enums.AddEditMode addEditMode;

        #endregion

        #region Constructors and Destructors

        /// <summary>Initializes a new instance of the <see cref="AddEditServer"/> class.</summary>
        /// <param name="runningAddEditMode">The running AddEditMode. </param>
        public AddEditServer(Enums.AddEditMode runningAddEditMode)
        {
            this.InitializeComponent();

            this.addEditMode = runningAddEditMode;

            if (runningAddEditMode == Enums.AddEditMode.Add)
            {
                this.txtServerTolerance.Text = "10";
            }else if(runningAddEditMode == Enums.AddEditMode.Edit)
            {
                TxtServerName.Enabled = false;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the txt server name.
        /// </summary>
        public TextBox TxtServerName
        {
            get
            {
                return this.txtServerName;
            }

            set
            {
                this.txtServerName = value;
            }
        }

        /// <summary>
        ///   Gets or sets the txt server tolerance.
        /// </summary>
        public TextBox TxtServerTolerance
        {
            get
            {
                return this.txtServerTolerance;
            }

            set
            {
                this.txtServerTolerance = value;
            }
        }

        /// <summary>
        ///   Gets or sets the txt server url.
        /// </summary>
        public TextBox TxtServerUrl
        {
            get
            {
                return this.txtServerURL;
            }

            set
            {
                this.txtServerURL = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   The validate values.
        /// </summary>
        /// <returns> The <see cref="bool" /> . </returns>
        public bool validateValues()
        {
            try
            {
                if (this.txtServerName.Text.Length == 0)
                {
                    MessageBox.Show("Server name cannot be blank.");
                    return false;
                }

                if (this.txtServerURL.Text.Length == 0)
                {
                    MessageBox.Show("Server Url to check cannot be blank.");
                    return false;
                }

                if (this.txtServerTolerance.Text.Length == 0)
                {
                    MessageBox.Show("Tolerance cannot be blank\nDefault is 10 seconds.");
                    return false;
                }

                int tolTest;
                if (int.TryParse(this.txtServerTolerance.Text, out tolTest) == false)
                {
                    MessageBox.Show("Not a valid value for tolerance.\nTolerance should be an integer representing seconds");
                    return false;
                }

                if (this.addEditMode == Enums.AddEditMode.Add)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

                    if (config.AppSettings.Settings[this.txtServerName.Text] != null)
                    {
                        MessageBox.Show("Server of that name already exists.");
                        return false;
                    }
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Methods

        /// <summary>The cancel_ click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        //private void Cancel_Click(object sender, EventArgs e)
        //{
        //    this.Close();
        //}

        public Server GetServerDetails()
        {
            Server server = new Server();
            server.Name = TxtServerName.Text;
            server.UrlToCheck = TxtServerUrl.Text;
            server.Tolerance = new TimeSpan(0, 0, Convert.ToInt32(this.txtServerTolerance.Text));

            return server;
        }

        public event EventHandler CancelButtonClicked;

        public void BtnCancelClick(object sender, EventArgs e)
        {
            //if (this.CancelButtonClicked != null)
                this.CancelButtonClicked(sender, e);
            this.Close();

        }
        public event EventHandler SaveButtonClicked;
        
        //public void BtnSaveClick(object sender, EventArgs e)
        //{
        //    if (this.validateValues())
        //    {
        //        Server server = new Server();
        //        server.Name = this.txtServerName.Text;
        //        server.UrlToCheck = this.txtServerURL.Text;
        //        server.Tolerance = new TimeSpan(0, 0, Convert.ToInt32(this.txtServerTolerance.Text));


                
        //        this.Close();
        //    }
        //}

        /// <summary>The save_ click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        /// <returns>The <see cref="Server"/> . </returns>
        private void BtnSaveClick(object sender, EventArgs e)
        {
            if (this.validateValues())
            {
                Server server = new Server();
                server.Name = this.txtServerName.Text;
                server.UrlToCheck = this.txtServerURL.Text;
                server.Tolerance = new TimeSpan(0, 0, Convert.ToInt32(this.txtServerTolerance.Text));

                this.Opacity = 0;



                if (this.SaveButtonClicked != null)
                    this.SaveButtonClicked(server, e);
                
                //if (AddEditMode == Enums.AddEditMode.Add)
                //{
                    
                //}

                //Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
                //config.AppSettings.Settings.Add(server.Name, server.Serialize());
                //config.Save(ConfigurationSaveMode.Full);
                //config.Save();



                // Add an Application Setting.

                // config.AppSettings.Settings.Add("ModificationDate",
                // DateTime.Now.ToLongTimeString() + " ");

                // Just a note for anyone reading this that config.Save(ConfigurationSaveMode.Modified) only saves the value if the key is predefined in the app.config file. To actually create a new one, you need to use just use config.Save() with no parameters. 

                // Dictionary<string, Server> serverList =
                // Settings.Default.ServerList.Deserialize<Dictionary<string, Server>>();

                // serverList.Add(this.txtServerName.Text, server);

                // Settings.Default.ServerList = serverList.Serialize();
                // Settings.Default.Save();
                this.Close();
            }
        }

        #endregion
    }
}

/*NOtes
 * 
 * 
 * 
 */
