﻿namespace ServerTimeChecker
{
    partial class EditServers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serverTimeControlList1 = new ServerTimeChecker.ServerTimeControlList();
            this.SuspendLayout();
            // 
            // serverTimeControlList1
            // 
            this.serverTimeControlList1.Location = new System.Drawing.Point(13, 13);
            this.serverTimeControlList1.MaximumSize = new System.Drawing.Size(445, 512);
            this.serverTimeControlList1.Name = "serverTimeControlList1";
            this.serverTimeControlList1.Size = new System.Drawing.Size(445, 512);
            this.serverTimeControlList1.TabIndex = 0;
            // 
            // EditServers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 532);
            this.Controls.Add(this.serverTimeControlList1);
            this.MaximumSize = new System.Drawing.Size(478, 570);
            this.Name = "EditServers";
            this.Text = "EditServers";
            this.ResumeLayout(false);

        }

        #endregion

        private ServerTimeControlList serverTimeControlList1;

    }
}