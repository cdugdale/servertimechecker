or (further to suggestion from Fredrick in comments)

System.Net.WebClient wc = new System.Net.WebClient();
string webData = wc.DownloadString("http://www.yoursite.com/resource/file.htm");
When you say it took 30 seconds, can you expand on that a little more? There are many reasons as to why th













from agenda




in master page
 <!-- Dynamic: DCC: Header -->
        <asp:Literal id="litTemplateHtmlHeader" runat="server" EnableViewState="false"></asp:Literal>
        <!-- Dynamic: DCC: /Header -->





 Protected Sub RenderTemplate()

            Try

                litTemplateHtmlHeader.Text = Common.Utilities.Design.GetDynamicHeaderHtml(HttpContext.Current, RegistrarsSettings.ClientDesignDynamicHeaderUrl)
                litTemplateHtmlFooter.Text = Common.Utilities.Design.GetDynamicFooterHtml(HttpContext.Current, RegistrarsSettings.ClientDesignDynamicFooterUrl)

            Catch ex As Exception
                Throw ex
            End Try

        End Sub





        Public Shared Function GetDynamicHeaderHtml( _
            ByVal context As HttpContext, _
            ByVal url As String _
        ) As String

            Const KEY As String = "DynamicHeaderHtml"

            Dim retVal As String = String.Empty
            Dim response As ResponseData = Nothing

            Try

                If (context.Cache.[Get](KEY) Is Nothing) OrElse (Convert.ToString(context.Cache.[Get](KEY)).Length = 0) Then
                    response = Utils.GetData(url)
                    context.Cache.Add( _
                        KEY, _
                        response.Content, _
                        Nothing, _
                        System.DateTime.Now.AddMinutes(60), _
                        System.Web.Caching.Cache.NoSlidingExpiration, _
                        System.Web.Caching.CacheItemPriority.BelowNormal, _
                        Nothing)
                End If

                retVal = Convert.ToString(context.Cache.[Get](KEY))
            Catch ex As Exception
                Throw ex
            End Try

            Return retVal

        End Function




        Public Shared Function GetData(ByVal url As String) As ResponseData

            Dim retVal As ResponseData = Nothing
            Dim uri As Uri = Nothing
            Dim request As WebRequest = Nothing
            Dim response As HttpWebResponse = Nothing
            Dim responseStream As Stream = Nothing
            Dim reader As StreamReader = Nothing

            Try

                retVal = New ResponseData()

                Try
                    If url.Trim().Length > 0 Then
                        uri = New Uri(url)
                    Else
                        retVal.StatusCode = HttpStatusCode.NotFound
                    End If
                Catch generatedExceptionName As UriFormatException
                    retVal.StatusCode = HttpStatusCode.NotFound
                End Try

                If retVal.StatusCode <> HttpStatusCode.NotFound Then

                    request = WebRequest.Create(url)

                    ' UNDONE: Credentials - http://www.devnewsgroups.net/group/microsoft.public.dotnet.framework/topic42654.aspx
                    request.Credentials = CredentialCache.DefaultCredentials

                    response = CType(request.GetResponse(), HttpWebResponse)

                    retVal.StatusCode = response.StatusCode

                    responseStream = response.GetResponseStream()
                    If response.ContentEncoding.ToLower().Contains("gzip") Then
                        responseStream = New GZipStream(responseStream, CompressionMode.Decompress)
                    ElseIf response.ContentEncoding.ToLower().Contains("deflate") Then
                        responseStream = New DeflateStream(responseStream, CompressionMode.Decompress)
                    End If

                    reader = New StreamReader(responseStream)

                    retVal.Content = reader.ReadToEnd()

                End If
            Catch ex As Exception
                Throw ex
            Finally

                If reader IsNot Nothing Then
                    reader.Close()
                    reader.Dispose()
                    reader = Nothing
                End If
                If responseStream IsNot Nothing Then
                    responseStream.Close()
                    responseStream.Dispose()
                    responseStream = Nothing
                End If
                If response IsNot Nothing Then
                    response.Close()
                    response = Nothing

                End If
            End Try

            Return retVal

        End Function